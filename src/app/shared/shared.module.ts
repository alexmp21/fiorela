import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarauthComponent } from './navbarauth/navbarauth.component';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [
    NavbarauthComponent
  ],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports:[
    NavbarauthComponent,
    RouterModule
  ]
})
export class SharedModule { }

import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';



const routes: Routes = [

    {path:'auth', loadChildren:() => import('./auth/auth.module').then((m)=>m.AuthModule) },

    {path:'web', loadChildren:() => import('./view/view.module').then((m)=>m.ViewModule) },

    {path:'**', pathMatch:'full', redirectTo:'auth'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{useHash:true})],
  exports: [RouterModule]
})
export class AppRoutingModule {}

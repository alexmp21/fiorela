import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ViewestrucutraComponent } from '../layouts/web/viewestrucutra/viewestrucutra.component';
import { ViewRoutingModule } from './view-routing.module';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [
    ViewestrucutraComponent
  ],
  imports: [
    CommonModule,
    // ruta view
    ViewRoutingModule,
    RouterModule
  ]
})
export class ViewModule { }

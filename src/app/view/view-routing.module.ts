import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { ViewestrucutraComponent } from '../layouts/web/viewestrucutra/viewestrucutra.component';
import { InicioComponent } from './inicio/inicio.component';


const routes: Routes = [
  { path: '', component: ViewestrucutraComponent,


    children:[
      {path: 'inicio', component: InicioComponent},
      {path:'**', pathMatch:'full', redirectTo:'inicio'}
    ]

},
  
  
 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ViewRoutingModule {}

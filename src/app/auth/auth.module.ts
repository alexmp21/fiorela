import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { RouterModule } from '@angular/router';
import { AuthestructuraComponent } from '../layouts/web/authestructura/authestructura.component';
import { AuthRoutingModule } from './auth-routing.module';
import { SharedModule } from '../shared/shared.module';



@NgModule({
  declarations: [
    LoginComponent,
    RegisterComponent,
    AuthestructuraComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    // rutas auth
    AuthRoutingModule,
    SharedModule
  ]
})
export class AuthModule { }

import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AuthestructuraComponent } from '../layouts/web/authestructura/authestructura.component';


const routes: Routes = [
  
  {path:'', component:AuthestructuraComponent,

    children:[
      {path:'login', component: LoginComponent},
      {path:'register', component: RegisterComponent},
      {path:'**', pathMatch:'full', redirectTo:'login'}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule {}

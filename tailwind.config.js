module.exports = {
  content: ["./src/**/*.{html,js}"],
  theme: {
    extend: {


      fontFamily: {
        Roboto: ['Roboto'],
        Poppins: ['Poppins'],
        Manrope: ['Manrope'],
        Aclonica: ['Aclonica'],
        Arimo:['Arimo'],
        Arizonia:['Arizonia'],
        Artifika:['Artifika'],
        Berkshire:['Berkshire'],
        Lobster:['Lobster'],
        LuckiestGuy:['Luckiest Guy'],
        OleoScript:['Oleo Script'],
        Tangerine: ['Tangerine']
      }
    },
  },
  plugins: [],
}